# -*- coding: utf-8 -*-
"""
Created on Sun Oct 23 22:03:57 2016

@author: manish
"""
from numpy.linalg import inv
import scipy
import scipy.linalg
import numpy as np   
# SciPy Linear Algebra Library
def LUdecompose(A):
    A_inv= inv(A)
    if np.linalg.det(A) !=0:
        print'matrix is invertible and the inverse is: \n' ,A_inv
        P, L, U = scipy.linalg.lu(A)
        print "\nP:\n", P,"\n \n \n L:\n", L, "\n \n \n  U:\n",U
A = scipy.array([ [7, 3, -1, 2], [3, 8, 1, -4], [-1, 1, 4, -1], [2, -4, -1, 6] ])
LUdecompose(A)