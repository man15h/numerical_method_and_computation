# -*- coding: utf-8 -*-
"""
Created on Sun Oct 23 19:08:02 2016

@author: manish
"""
from __future__ import division
from numpy.linalg import inv
import numpy as np
def gaussianElimination(A, b):
    '''
    Gaussian elimination with no pivoting.
    % input: A is an n x n nonsingular matrix
    %        b is an n x 1 vector
    % output: x is the solution of Ax=b.
    % post-condition: A and b have been modified. 
    '''
    n =  len(A)
    A_inv= inv(A)
    print 'matrix A is:\n',A
    print 'matrix b is\n',b 
    if np.linalg.det(A) !=0:
        print'matrix is invertible and the inverse is: \n' ,A_inv
    if b.size != n:
        raise ValueError("Invalid argument: incompatible sizes between A & b.", b.size, n)
    for i in range(n-1):
        for j in range(i+1, n):
            multiplier = A[j][i]/A[i][i]
            #the only one in this column since the rest are zero
            A[j][i] = multiplier
            for k in range(i + 1, n):
                A[j][k] = A[j][k] - multiplier*A[i][k]
            #Equation solution column
            b[j] = b[j] - multiplier*b[i]
    x = np.zeros(n)
    k = n-1
    while k >= 0:
        x[k] = (b[k] - np.dot(A[k,k+1:],x[k+1:]))/A[k,k]
        k = k-1
    print 'Solution is: \n',x
A = np.array([[-3.,2.,-6.],[5.,7.,-5.],[1.,4.,-2.]])
b =  np.array([[6.],[6.],[8.]])
gaussianElimination(np.copy(A), np.copy(b))