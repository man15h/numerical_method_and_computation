# -*- coding: utf-8 -*-
"""
Created on Sun Oct 23 22:22:17 2016

@author: manish
"""

import numpy as np
def cholDecom(A):
    if np.all(np.linalg.eigvals(A) > 0):
        print'it is a positive symmetric matrix\n and Cholesky decomposition (lower triangular matrix) is: \n ',np.linalg.cholesky(A)
    else:
        print 'it is not a positive symmetric matrix'
A = np.array([[4.,12.,-16.],[12.,37.,-43.],[-16.,-43.,98.]])
cholDecom(A)
