%   MANISH KUMAR MEENA
%   2013ME10692
%   Assignment 1


% call ex1_function(f,df,x);

function x = ex1_function(f,df,x)

    %Default Values
    max_iter = 100; %maximum number of iteration
    TOL= 1e-10; %Tolerance
    %nothing better than a while loop
    for t=1:max_iter
        x1=x-f(x)/df(x);
        if abs(x1-x)<TOL;
            fprintf('\nIteration #%d: x=%.20f y=%.20f', t, x1,f(x));
            fprintf('method not converging')
            y(t)= f(x1);  % save function values as vector
            break
        else
            fprintf('\nIteration #%d: x=%.20f y=%.20f', t, x,f(x));
            y(t)= f(x); % save function values as vector
            x=x1;
        end
    end
     plot(1:t,y,'bd-')
    grid
    if t>=max_iter 
        fprintf('method not converging');
    end
end
