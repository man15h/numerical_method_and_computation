%   MANISH KUMAR MEENA
%   2013ME10692
%   Assignment 4


function  [L,U,P]= ex4_func(A)
    %Size of coefficient matrix and check if matrix is square matrix
    [size_row ,size_col] = size ( A );
    if ( size_row ~= size_col )
       error( 'Square coefficient matrix required' );
    end;
    if (abs(det(A))<1e-12); %machine precision
       error( 'Error: coefficient matrix is singular' );
    end
    % Calling inbuilt matlab function
    [L,U,P] = lu(A);
    fprintf ('Lower triangular matrix L:\n');disp (L);
    fprintf ('Upper triangular matrix U:\n');disp (U);
    fprintf ('Permutaion matrix P:\n');disp (P);
end
