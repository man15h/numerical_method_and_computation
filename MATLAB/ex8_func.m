%   MANISH KUMAR MEENA
%   2013ME10692
%   EXCERCISE 8

function f = ex8_func ( X,Y,U)
n = length ( X );
m = length ( Y );
if ( n ~= m )
   error( 'number of ordinates and number of function values must be equal' )
end
%Divided-difference computaion
f = Y;
for j = 2:n
    % this loop will be iterable as follows for i = first : increment : last
    % example i= 4:-1:2
    for i = n:-1:j
	    f(i) = ( f(i) - f(i-1) ) / ( X(i) - X(i-j+1) );
    end
end
%print function value corresponding to orrdinates value
n   = length ( U );
% degree of the interpolationg function
deg = length(f) - 1;
% ones(1,n) creating an array of value 1
Y = f(deg+1) * ones(1,n);

% this loop will be iterable as follows for i = first : increment : last
for j = deg : -1 : 1
	Y = Y .* ( U - X(j) * ones(1,n) ) + f(j) * ones(1,n);
end
z=length (f);
for j = 1 :z
   fprintf( 'f(%d,1):  %10f \n', j, f(j) );
end;
disp('Interpolating values U:');
disp(Y);
