% MANISH KUMAR MEENA
% 2013me10692
% Assignment 6
% http://marta.certik.cz/NM/Asor.pdf

function x = ex6_func( A ,b,x)
    %verifying the sufficient condition
    [A_row , A_column ] = size (A);
    if A_row ~= A_column
        error('matrix A should be a square matrix')
    end
    [b_row , b_column ] = size (b);
    if b_row ~= A_row || b_column~=1
       error( 'ERROR:input error..pls re-enter data')
    end

    %Default Input
    tol = 1e-4;
    maxiter=100;
    iter = 0;
    omega=1.25;
    n=length(x);
    while (iter<maxiter)
        %saving old x value for error calculation
        x_old=x;
        for i=1:n
            I = [1:i-1 i+1:n];
            x(i) = (1-omega)*x(i)+omega/A(i,i)*( b(i)-A(i,I)*x(I) );
        %    disp(x)
        end
        if(norm(x-x_old)/norm(x)<tol)
            disp(x)
            break
        end
        iter = iter+1;
    end
end
