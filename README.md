# PROJECTS : MTL107 (2016 Sem 1) #

## Numerical Method and Computaion ##
Each assignment has one file. It contain the main function definition and will generate desired output with given input.There is lots of comments in the code for explanation.
### 1 MATLAB ###
containing Main Function Value. Desired in[put should be given using MATLAB script file. there are example Input files for better understanding
### 2 Python ###
Have main function with inputs, change the input for desired output

** Assignment 1) **

Newtons Method: Implement Newtons method to find a root of a function f : R → R. The input arguments of the function should be initial guess, function f and its derivative f. The code should print all the iteration on the screen, tell wether method is converging or not and print final solution to the screen. Plot the  value of the function after each iteration to show the results. In case we are not converging to the root, method should stop after some iterations.

** Assignment 2) **

Secant Method: Implement Secant method to find a root of a function f : R →R. The input arguments of the function should be two initial guesses and function f. The code should print all the iteration on the screen, tell wether method is con-verging or not and print final solution to the screen. In case we are not converging to the root, method should stop after some iterations.

** Assignment 3) **

Gauss Elimination and Back Substituting: Implement the well known Gauss elimination and Backward substitution method to solve a system of linear equations. Your input variables are a matrix A and right hand side vector b. Your code should be able decide if matrix is invertible or not and if yes then the code should print the solutions of the linear system.

**  Assignment 4) **

LU Decomposition: Implement LU decomposition of a matrix. Input should be a matrix. Your code should be able to check if matrix is invertible. Output should be permutation matrix P, lower triangular matrix L and upper triangular matrix U.

**  Assignment 5) **

LLT Decomposition: Implement Cholesky decomposition of a symmetric positive matrix. Input should be a matrix. Your code should be able to check if matrix is symmetric positive definite. Output should lower triangular matrix L

**  Assignment 6) **

SOR: Implement SOR method with pre-conditioner. Input should be matrix A, a and initial guess. Specify appropriate stopping and exit criteria’s with warnings.

**  Assignment 7) **

CG: Implement Conjugate gradient method with pre-conditioner. Input should be matrix A, a preconditioner and initial guess. Specify appropriate stopping and exit criteria’s with warning** s


**In next three assignments, input variables for the MATLAB function is a column vector X (where values are known), a column vector Y ( values of the unknown function at X) of same size as X, and a column vector U (whose min and max values lies between min and max of X). Here we want to calculate the approximated values of unknown function. This MATLAB function should give a vector V (same size as U) as output.**

** Exercise 8 **

Write a MATLAB function to Implement Newton divided difference method to obtain the interpolating value V at U.

** Exercise 9 **

Write a MATLAB function to Implement piecewise linear interpolation method to obtain the interpolating value V at U.

** Exercise 10 **

Write a MATLAB function to Implement natural cubic spline method to obtain the interpolating value V at U.